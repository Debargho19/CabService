package com.dev.api;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import com.dev.healthcheck.ServiceHealthCheck;

import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;


public class CABApp extends Application<Configuration> {
 
    @Override
    public void initialize(Bootstrap<Configuration> b) {
    }
 
    @Override
    public void run(Configuration configuration, Environment environment) throws Exception {
        AnnotationConfigWebApplicationContext parent = new AnnotationConfigWebApplicationContext();
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();

        parent.refresh();
        parent.getBeanFactory().registerSingleton("configuration", configuration);
        parent.registerShutdownHook();
        parent.start();

        ctx.setParent(parent);
        ctx.register(CABConfiguration.class);
        ctx.refresh();
        ctx.registerShutdownHook();
        ctx.start();
        environment.servlets().addServletListeners(new ContextLoaderListener(ctx));
        
        environment.jersey().register(ctx.getBean(CabServiceController.class));
        environment.healthChecks().register("APIHealthCheck", new ServiceHealthCheck());
    }
 
    public static void main(String[] args) throws Exception {
        new CABApp().run(args);
    }
}