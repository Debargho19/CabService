package com.dev.api;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.pojo.impl.DropPoint;
import com.dev.pojo.impl.RoutePlan;
import com.dev.pojo.impl.request.CabAddRequest;
import com.dev.pojo.impl.request.DropPointRequest;
import com.dev.pojo.impl.request.RegisterMemberRequest;
import com.dev.service.impl.CabRegistryService;
import com.dev.service.impl.CabService;
import com.dev.service.impl.DropPointService;
import com.dev.service.impl.RouteService;

import ch.qos.logback.classic.Logger;

@Path("/cabService")
@Produces(MediaType.APPLICATION_JSON)
@Service
public class CabServiceController {
	private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

	@Autowired
	CabService cabService;

	@Autowired
	CabRegistryService cabRegistryService;

	@Autowired
	DropPointService dropPointService;
	
	@Autowired
	RouteService routeService;


	@POST
	@Path("/register")
	public Response addTeamMember(RegisterMemberRequest s) {
		LOGGER.info("Started logging !!");
		try {
			cabRegistryService.save(s);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.status(Response.Status.CREATED).build();
	}

	@POST
	@Path("/cabs")
	public Response addCab(CabAddRequest s) {
		cabService.save(s);
		return Response.status(Response.Status.CREATED).build();
	}

	@POST
	@Path("/drop_points")
	public Response uploadDropPoints(Map<String, String> map) {
		try {
			DropPointRequest s = new DropPointRequest();
			Map<String, String> distMap = new LinkedHashMap<>();
			Set<Entry<String, String>> entrySet = map.entrySet();
			for (Entry<String, String> entry : entrySet) {
				distMap.put(entry.getKey(), entry.getValue());
			}
			s.setDropPointsDistance(distMap);
			dropPointService.save(s);
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.status(Response.Status.CREATED).build();
	}

	@GET
	@Path("/getDropPoints")
	public Response getCabs() {
		List<DropPoint> dropPoints = null;
		try {
			dropPoints = dropPointService.getAll();
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.ok(dropPoints).build();
	}

	@GET
	@Path("/route_plan")
	public Response getRoutePlan() {
		RoutePlan route = null;
		try {
			route = routeService.get(null);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.ok(route).build();
	}

}
