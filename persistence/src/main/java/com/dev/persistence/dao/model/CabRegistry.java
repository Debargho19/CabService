package com.dev.persistence.dao.model;


public class CabRegistry{

	private String teamMemberId;
	private String gender;
	private String dropPoint;
	
	public String getTeamMemberId() {
		return teamMemberId;
	}
	public void setTeamMemberId(String teamMemberId) {
		this.teamMemberId = teamMemberId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDropPoint() {
		return dropPoint;
	}
	public void setDropPoint(String dropPoint) {
		this.dropPoint = dropPoint;
	}
	
}
