package com.dev.persistence.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

import com.dev.persistence.dao.model.CabRegistry;

@Service
public class CabRegistryDao {
	private static Map<String,List<CabRegistry>> inMemoryCabRegistryDataByDropPoint = new HashMap<>();
	private static Map<String,CabRegistry> inMemoryCabRegistryData = new HashMap<>();
	private static int MALE_COUNT = 0;
	private static int FEMALE_COUNT = 0;
	
	
	public void addCab(CabRegistry cabRegistry) {
		List<CabRegistry> existingEntries = inMemoryCabRegistryDataByDropPoint.get(cabRegistry.getDropPoint());
		if(existingEntries == null ) {
			existingEntries = new ArrayList<>();
		}
		existingEntries.add(cabRegistry);
		inMemoryCabRegistryDataByDropPoint.put(cabRegistry.getDropPoint(), 
				existingEntries);
		inMemoryCabRegistryData.put(cabRegistry.getTeamMemberId(), cabRegistry);
	}
	
	public void addAll(List<CabRegistry> cabregistries) {
		for (CabRegistry cabRegistry : cabregistries) {
			addCab(cabRegistry);
			if("F".equalsIgnoreCase(cabRegistry.getGender())){
				FEMALE_COUNT++;
			}else {
				MALE_COUNT++;
			}
		}
	}

	public List<CabRegistry> getByID(String dropPoint) {
		return inMemoryCabRegistryDataByDropPoint.get(dropPoint);
	}
	
	public Map<String,Integer> getDropPointWithMemberCount(){
		Map<String,Integer> dropPointMap = new HashMap<>();
		for (Entry<String,List<CabRegistry>> cabRegistryByDropPoint : inMemoryCabRegistryDataByDropPoint.entrySet()) {
			dropPointMap.put(cabRegistryByDropPoint.getKey(), cabRegistryByDropPoint.getValue().size());
		}
		return dropPointMap;
	}
	public Map<String,List<CabRegistry>> getCabRegistrybyDropPoints(){
		return inMemoryCabRegistryDataByDropPoint;
	}
	
	public Map<String,CabRegistry> getAll(){
		return inMemoryCabRegistryData;
	}
	
	public boolean isAllFemaleMembers() {
		return (FEMALE_COUNT>0 && MALE_COUNT==0);
	}

}
