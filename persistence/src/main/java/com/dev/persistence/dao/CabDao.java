package com.dev.persistence.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

import com.dev.persistence.dao.model.Cabs;

@Service
public class CabDao {
	private static Map<String,Cabs> inMemoryCabData = new HashMap<>();
	
	public void addCab(Cabs cab) {
		inMemoryCabData.put(cab.getId(), cab);
	}
	
	public void addAll(List<Cabs> cabs) {
		for (Cabs cab : cabs) {
			inMemoryCabData.put(cab.getId(), cab);
		}
	}

	public List<Cabs> getAll() {
		List<Cabs> cabs = new ArrayList<>();
		for (Entry<String, Cabs> cabEntry : inMemoryCabData.entrySet()) {
			cabs.add(cabEntry.getValue());
		}
		return cabs;
	}
	
	public Cabs getByID(String cabID) {
		return inMemoryCabData.get(cabID);
	}
	
	public Map<String,Integer> getCabsWithCapacity(){
		 Map<String,Integer> cabsWithCapacity= new HashMap<>();
		 for (Entry<String,Cabs> cabsData : inMemoryCabData.entrySet()) {
			 cabsWithCapacity.put(cabsData.getKey(), cabsData.getValue().getCapacity());
		}
		 return cabsWithCapacity;
	}
	
	public Map<String,Integer> getCabsWithCost(){
		 Map<String,Integer> cabsWithCapacity= new HashMap<>();
		 for (Entry<String,Cabs> cabsData : inMemoryCabData.entrySet()) {
			 cabsWithCapacity.put(cabsData.getKey(), cabsData.getValue().getCost());
		}
		 return cabsWithCapacity;
	}

	public static Map<String, Cabs> getInMemoryCabData() {
		return inMemoryCabData;
	}

	public static void setInMemoryCabData(Map<String, Cabs> inMemoryCabData) {
		CabDao.inMemoryCabData = inMemoryCabData;
	}
	
}
