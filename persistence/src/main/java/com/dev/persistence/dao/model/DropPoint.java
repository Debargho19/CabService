package com.dev.persistence.dao.model;

import java.util.Map;


public class DropPoint{

	private String dropPointName;
	private Map<String,Integer> distance;
	
	public String getDropPointName() {
		return dropPointName;
	}
	public void setDropPointName(String dropPointName) {
		this.dropPointName = dropPointName;
	}
	public Map<String, Integer> getDistance() {
		return distance;
	}
	public void setDistance(Map<String, Integer> distance) {
		this.distance = distance;
	}
	
}
