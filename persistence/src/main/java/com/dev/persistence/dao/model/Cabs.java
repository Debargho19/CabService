package com.dev.persistence.dao.model;

public class Cabs{

	private String id;
	private Integer cost;
	private Integer capacity;
	
	public Cabs(String id, Integer cost, Integer capacity) {
		super();
		this.id = id;
		this.cost = cost;
		this.capacity = capacity;
	}
	
	public Cabs() {
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	
}
