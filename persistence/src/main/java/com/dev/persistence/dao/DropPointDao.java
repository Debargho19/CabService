package com.dev.persistence.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

import com.dev.persistence.dao.model.DropPoint;

@Service
public class DropPointDao {
	private static Map<String,DropPoint> inMemoryDropPointData = new HashMap<>();
	
	public void addDropPoint(DropPoint dropPoint) {
		inMemoryDropPointData.put(dropPoint.getDropPointName(), dropPoint);
	}
	
	public void addAll(List<DropPoint> dropPoints) {
		for (DropPoint dropPoint : dropPoints) {
			inMemoryDropPointData.put(dropPoint.getDropPointName(), dropPoint);
		}
	}

	public List<DropPoint> getAll() {
		List<DropPoint> dropPoints = new ArrayList<>();
		for (Entry<String, DropPoint> cabEntry : inMemoryDropPointData.entrySet()) {
			dropPoints.add(cabEntry.getValue());
		}
		return dropPoints;
	}
	
	public DropPoint getByID(String dropPointName) {
		return inMemoryDropPointData.get(dropPointName);
	}
	
	public Map<String, DropPoint> getDropPointMap() {
		return inMemoryDropPointData;
	}

}
