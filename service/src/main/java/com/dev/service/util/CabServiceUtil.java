package com.dev.service.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.dev.persistence.dao.model.CabRegistry;
import com.dev.persistence.dao.model.DropPoint;
import com.dev.pojo.impl.Gender;
import com.dev.pojo.impl.MinPathWrapperVO;

public class CabServiceUtil {
	/**
	 * @param cabMemberList
	 * @param dropPointMap
	 * @param allRegistries
	 * @param isAllFemaleMembers 
	 * @return
	 * Find min path by iterating all possible permuted member drop order
	 **/
	public static MinPathWrapperVO getMinPath(List<String> cabMemberList, Map<String, DropPoint> dropPointMap,
			Map<String, CabRegistry> allRegistries, boolean isAllFemaleMembers) {
		Map<String, Integer> minPath = new LinkedHashMap<>();
		int localMin = Integer.MAX_VALUE;
		boolean isAllCabMembersFemale = isAllCabMembersFemale(cabMemberList,allRegistries);
		if(isAllCabMembersFemale && !isAllFemaleMembers) {
			MinPathWrapperVO minPathWrapper = new MinPathWrapperVO();
			minPathWrapper.setAllCabMembersFemale(true);
			return minPathWrapper;
		}
		List<List<String>> allMemberPermutation = getAllMemberPermutations(cabMemberList);

		for (List<String> memberDropOrder : allMemberPermutation) {
			if(!isAllFemaleMembers && Gender.F.getValue().equalsIgnoreCase(allRegistries.get(memberDropOrder.get(memberDropOrder.size()-1)).getGender()))
				continue;
			DropPoint header = dropPointMap.get("target_headquarter");
			Map<String, Integer> distance = header.getDistance();
			
			int distSum = 0;
			Map<String, Integer> localMinPath = new LinkedHashMap<>();
			for (String cabMembers : memberDropOrder) {
				int dist = distance.get(allRegistries.get(cabMembers).getDropPoint());
				localMinPath.put(cabMembers, dist);
				header = dropPointMap.get(allRegistries.get(cabMembers).getDropPoint());
				distance = header.getDistance();
				distSum += dist;
			}
			if (distSum < localMin) {
				localMin = distSum;
				minPath = localMinPath;
			}
		}
		MinPathWrapperVO minPathWrapper = new MinPathWrapperVO();
		minPathWrapper.setCost(localMin);
		minPathWrapper.setMinPath(minPath);
		return minPathWrapper;
	}
	
	private static boolean isAllCabMembersFemale(List<String> cabMemberList, Map<String, CabRegistry> allRegistries) {
		boolean isAllCabMemberFemale = true;
		for (String cabMember : cabMemberList) {
			if("M".equalsIgnoreCase(allRegistries.get(cabMember).getGender()))
				isAllCabMemberFemale = false;
		}
		return isAllCabMemberFemale;
	}

	/**
	 * 
	 * @param members
	 * @param cabs
	 * @return
	 * This method is supposed to return all possible ways in which cabs C1,C2 can be allocated to members M1,M2,M3
	 */
	public static List<Map<String,String>> getAllCabCombinations(List<String> members, List<String> cabs) {
		if (members.isEmpty()) {
			Map<String,String> empty = Collections.emptyMap();
            return Collections.singletonList(empty);
        } else {
        		String member = members.get(0);
            List<Map<String,String>> subCombination = getAllCabCombinations(members.subList(1,members.size()), cabs);

            List<Map<String,String>> solutions = new LinkedList<>();
            for (String cab: cabs) {
                for (Map<String,String> combination : subCombination) {
                		Map<String,String> combinationMap = new HashMap<>(combination);
                    combinationMap.put(member, cab);
                    solutions.add(combinationMap);
                }
            }
            return solutions;
        }
	}
	
	
	/**
	 *  Filter combinations based on cab capacity.
	 *  So from all the possible combinations remove options which exceeds seat allocation exceeding the cab capacity 
	 */
	public static List<Map<String, String>> filterCabCombinations(List<Map<String, String>> allCabCombinationsForMembers,
			Map<String, Integer> cabsWithCapacity,boolean isAllFemaleMembers) {
		List<Map<String, String>> finalListOfPossibilities = new LinkedList<>();
		for (Map<String, String> map : allCabCombinationsForMembers) {
			Map<String,Integer> combinationCapacity = new HashMap<>();
			boolean isCombinationFeasible = true;
			for (Entry<String, String> mapEntry : map.entrySet()) {
				int capacity = combinationCapacity.containsKey(mapEntry.getValue())?combinationCapacity.get(mapEntry.getValue())+1:1;
				combinationCapacity.put(mapEntry.getValue(), capacity);
				if(capacity > (cabsWithCapacity.get(mapEntry.getValue()) - (isAllFemaleMembers?1:0))) {
					isCombinationFeasible = false;
					break;
				}
			}
			if(isCombinationFeasible) {
				finalListOfPossibilities.add(map);
			}
		}
		return finalListOfPossibilities;
	}

	/**
	 * 
	 * @param members
	 * @return
	 * 
	 * This method returns all permutations of order in which members of a cab can be dropped
	 * if i/p is {A,B,C} -- o/p would be ABC ACB BAC BCA CAB CBA
	 */
	public static List<List<String>> getAllMemberPermutations(List<String> members) {
		if (members.isEmpty()) {
			List<String> empty = Collections.emptyList();
			return Collections.singletonList(empty);
		} else {
			List<List<String>> solutions = new LinkedList<>();
			for (String string : members) {
				List<String> modifiedMembers = new LinkedList<>(members);
				modifiedMembers.remove(string);
				List<List<String>> subPermutation = getAllMemberPermutations(modifiedMembers);
				for (List<String> permutation : subPermutation) {
					List<String> m = new LinkedList<>();
					m.add(string);
					m.addAll(permutation);
					solutions.add(m);
				}
			}
			return solutions;
		}
	}

}
