package com.dev.service.api;

import java.io.Serializable;
import java.util.List;

public interface IfService<T extends Serializable,S extends Serializable>{
	void save(S s)  throws Exception;
	T get(S s) throws Exception;
	void delete(S s)  throws Exception;
	List<T> getAll() throws Exception;
	List<T> getAllById(S s) throws Exception;
}
