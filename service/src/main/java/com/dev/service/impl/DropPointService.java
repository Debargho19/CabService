package com.dev.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.dev.persistence.dao.DropPointDao;
import com.dev.pojo.impl.DropPoint;
import com.dev.pojo.impl.request.DropPointRequest;
import com.dev.service.api.IfService;

@Service
@Scope( proxyMode = ScopedProxyMode.TARGET_CLASS )
public class DropPointService implements IfService<DropPoint, DropPointRequest> {
	
	@Autowired
	DropPointDao dropPointDao;

	@Override
	public void save(DropPointRequest s) throws Exception{
		Map<String,String> dropPointsDistance = s.getDropPointsDistance();
		List<com.dev.persistence.dao.model.DropPoint> dropPointsToSave = new ArrayList<>();
		List<String> dropPoints = new LinkedList<>();
		for (Entry<String, String> dropPoint : dropPointsDistance.entrySet()) {
			if(!"target_headquarter".equalsIgnoreCase(dropPoint.getKey()))
				dropPoints.add(dropPoint.getKey());
		}
		int size = dropPointsDistance.size();
		for (Entry<String, String> dropPointEntries : dropPointsDistance.entrySet()) {
			com.dev.persistence.dao.model.DropPoint dropPoint = new com.dev.persistence.dao.model.DropPoint();
			dropPoint.setDropPointName(dropPointEntries.getKey());
			Map<String,Integer> pointToPointDistance = new HashMap<>();
			int start = 0;
			String[] distList = dropPointEntries.getValue().split(",");
			if(distList.length != size-1) {
				throw new Exception("Invalid/ Insufficient entries for drop point distance map !!!");
			}
			for (String distance : distList) {
				pointToPointDistance.put(dropPoints.get(start), Integer.parseInt(distance));
				start++;
			}
			dropPoint.setDistance(pointToPointDistance);
			dropPointsToSave.add(dropPoint);
		}
		dropPointDao.addAll(dropPointsToSave);
	}

	@Override
	public DropPoint get(DropPointRequest s) {
		return null;
	}

	@Override
	public void delete(DropPointRequest s) {
		
	}

	@Override
	public List<DropPoint> getAll() {
		List<com.dev.persistence.dao.model.DropPoint> daoDropPoints =  dropPointDao.getAll();
		List<DropPoint> dropPoints = new ArrayList<>();
		for (com.dev.persistence.dao.model.DropPoint dropPoint : daoDropPoints) {
			DropPoint dropPointToReturn = new DropPoint();
			BeanUtils.copyProperties(dropPoint, dropPointToReturn);
			dropPoints.add(dropPointToReturn);
		}
		return dropPoints;
	}

	@Override
	public List<DropPoint> getAllById(DropPointRequest s) {
		return null;
	}

}
