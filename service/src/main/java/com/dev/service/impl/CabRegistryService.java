package com.dev.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.dev.persistence.dao.CabRegistryDao;
import com.dev.persistence.dao.DropPointDao;
import com.dev.persistence.dao.model.DropPoint;
import com.dev.pojo.impl.CabRegistry;
import com.dev.pojo.impl.Gender;
import com.dev.pojo.impl.request.RegisterMemberRequest;
import com.dev.service.api.IfService;

@Service
@Scope( proxyMode = ScopedProxyMode.TARGET_CLASS )
public class CabRegistryService implements IfService<CabRegistry, RegisterMemberRequest> {

	@Autowired
	private CabRegistryDao cabRegistryDao;
	
	@Autowired
	private DropPointDao dropPointDao;

	@Override
	public void save(RegisterMemberRequest s) throws Exception{
		// Get all Drop Point distance graph
		Map<String, DropPoint> dropPointMap = dropPointDao.getDropPointMap();
		if(dropPointMap.size() == 0) {
			throw new Exception("Drop points not defined yet !!!");
		}
		List<CabRegistry> cabregistry = s.getTeamMembers();
		List<com.dev.persistence.dao.model.CabRegistry> daoCabRegistries = new ArrayList<>();
		for (CabRegistry requestCabRegistry : cabregistry) {
			if(!dropPointMap.containsKey(requestCabRegistry.getDropPoint())) {
				throw new Exception("Drop point "+ requestCabRegistry.getDropPoint() + " does not exist!!!");
			}
			com.dev.persistence.dao.model.CabRegistry daoCabRegistry = new com.dev.persistence.dao.model.CabRegistry();
			BeanUtils.copyProperties(requestCabRegistry, daoCabRegistry);
			daoCabRegistry.setGender(requestCabRegistry.getGender().getValue());
			daoCabRegistries.add(daoCabRegistry);
		}
		cabRegistryDao.addAll(daoCabRegistries);
		
	}

	@Override
	public CabRegistry get(RegisterMemberRequest s) {
		return null;
	}

	@Override
	public void delete(RegisterMemberRequest s) {
		
	}

	@Override
	public List<CabRegistry> getAll() {
		return null;
	}

	@Override
	public List<CabRegistry> getAllById(RegisterMemberRequest s) {
		List<CabRegistry> cabregistry = s.getTeamMembers();
		List<CabRegistry> cabregistryList = new ArrayList<>();
		for (CabRegistry cabRegistry : cabregistry) {
			List<com.dev.persistence.dao.model.CabRegistry> daoCabRegistries = cabRegistryDao.getByID(cabRegistry.getDropPoint());
			List<CabRegistry> tempList = new ArrayList<>();
			for (com.dev.persistence.dao.model.CabRegistry daoCabRegistry : daoCabRegistries) {
				CabRegistry requestCabRegistry = new CabRegistry();
				BeanUtils.copyProperties(daoCabRegistry, requestCabRegistry);
				requestCabRegistry.setGender(Gender.getEnum(daoCabRegistry.getGender()));
				tempList.add(requestCabRegistry);
			}
			cabregistryList.addAll(tempList);
		}
		
		return cabregistryList;
	}
	

}
