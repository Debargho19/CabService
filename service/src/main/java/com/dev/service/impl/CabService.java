package com.dev.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.dev.persistence.dao.CabDao;
import com.dev.pojo.impl.Cabs;
import com.dev.pojo.impl.request.CabAddRequest;
import com.dev.service.api.IfService;

@Service
@Scope( proxyMode = ScopedProxyMode.TARGET_CLASS )
public class CabService implements IfService<Cabs, CabAddRequest>{

	@Autowired
	private CabDao cabDao;

	@Override
	public void save(CabAddRequest s) {
		List<Cabs> cabs = s.getCabs();
		List<com.dev.persistence.dao.model.Cabs> daoCabs = new ArrayList<>();
		for (Cabs requestCab : cabs) {
			com.dev.persistence.dao.model.Cabs daoCab = new com.dev.persistence.dao.model.Cabs();
			BeanUtils.copyProperties(requestCab, daoCab);
			daoCabs.add(daoCab);
		}
		cabDao.addAll(daoCabs);
	}

	@Override
	public Cabs get(CabAddRequest s) {
		return null;
	}

	@Override
	public void delete(CabAddRequest s) {
		
	}

	@Override
	public List<Cabs> getAll() {
		List<Cabs> cabs = new ArrayList<>();
		List<com.dev.persistence.dao.model.Cabs> daoCabs = cabDao.getAll();
		for (com.dev.persistence.dao.model.Cabs daoCab : daoCabs) {
			Cabs cab = new Cabs();
			BeanUtils.copyProperties(daoCab, cab);
			cabs.add(cab);
		}
		return cabs;
	}

	@Override
	public List<Cabs> getAllById(CabAddRequest s) {
		return null;
	}
	

}
