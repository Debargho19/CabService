package com.dev.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.dev.persistence.dao.CabDao;
import com.dev.persistence.dao.CabRegistryDao;
import com.dev.persistence.dao.DropPointDao;
import com.dev.persistence.dao.model.CabRegistry;
import com.dev.persistence.dao.model.DropPoint;
import com.dev.pojo.impl.MinPathWrapperVO;
import com.dev.pojo.impl.Route;
import com.dev.pojo.impl.RoutePlan;
import com.dev.service.api.IfService;
import com.dev.service.util.CabServiceUtil;

@Service
@Scope( proxyMode = ScopedProxyMode.TARGET_CLASS )
public class RouteService implements IfService<RoutePlan, RoutePlan> {

	@Autowired
	private CabRegistryDao cabRegistryDao;
	
	@Autowired
	private DropPointDao dropPointDao;
	
	@Autowired
	private CabDao cabDao;
	
	
	@Override
	public void save(RoutePlan s) {
	}

	@Override
	public RoutePlan get(RoutePlan s) throws Exception{
		//Get List of all Cabs available
		Map<String,Integer> cabsWithCost = cabDao.getCabsWithCost();
		if(cabsWithCost.size() == 0) {
			throw new Exception("No Cabs Added !!!");
		}
		Map<String,Integer> cabsWithCapacity = cabDao.getCabsWithCapacity();
		
		//Get Unique cab registries
		Map<String,CabRegistry>  allRegistries = cabRegistryDao.getAll();
		if(allRegistries.size() == 0) {
			throw new Exception("No Registries done yet !!!");
		}
		boolean isAllFemaleMembers = cabRegistryDao.isAllFemaleMembers();
		
		Set<String> uniqueMemberRegistries = allRegistries.keySet();
		// Get all Drop Point distance graph
		Map<String, DropPoint> dropPointMap = dropPointDao.getDropPointMap();
		if(dropPointMap.size() == 0) {
			throw new Exception("Drop points not defined yet !!!");
		}
		
		//Get all possible combinations of cabs that can be allocated for members
		List<Map<String,String>> allCabCombinationsForMembers = CabServiceUtil.getAllCabCombinations(new LinkedList<>(uniqueMemberRegistries),new LinkedList<>(cabsWithCost.keySet()));
		
		//Filter out possibilities which don't fit the cab size
		allCabCombinationsForMembers = CabServiceUtil.filterCabCombinations(allCabCombinationsForMembers,cabsWithCapacity,isAllFemaleMembers);
		
		if(allCabCombinationsForMembers == null || allCabCombinationsForMembers.size() == 0) {
			throw new Exception("Not enough Cabs !!!");
		}
		
		int minCost = Integer.MAX_VALUE;
		List<MinPathWrapperVO> pathWiseCost = null; 
		
		
		for (Map<String, String> map : allCabCombinationsForMembers) {
			// Need to find per cab best route and hence get a cab wise member lists who are traveling in it
			Map<String,List<String>> cabToMembersMap = getCabToRegisteredMemberMap(map);
			int cost = 0;
			Map<String,String> localMincablistToDropPoints = new HashMap<>();
			List<MinPathWrapperVO> localMinPathWrappers = new LinkedList<>();
			
			// Iterate and find combined cost of cab distribution and select min from that
			for (Entry<String,List<String>> cabWiseMembers : cabToMembersMap.entrySet()) {
				MinPathWrapperVO pathWrapper = CabServiceUtil.getMinPath(cabWiseMembers.getValue(),dropPointMap,allRegistries,isAllFemaleMembers);
				
				//To handle case where a combination has all members as Female and all registered employees are not female -- Since female cannot be be last drop
				if(pathWrapper.isAllCabMembersFemale() && !isAllFemaleMembers) {
					localMinPathWrappers.clear();
					break;
				}
				pathWrapper.setCabId(cabWiseMembers.getKey());
				localMinPathWrappers.add(pathWrapper);
				
				Map<String,Integer> minPath = pathWrapper.getMinPath();
				for (Entry<String,Integer> minPathElements : minPath.entrySet()) {
					localMincablistToDropPoints.put(minPathElements.getKey(),cabWiseMembers.getKey());
					cost+=(minPathElements.getValue()*(cabsWithCost.get(cabWiseMembers.getKey())));
				}
			}
			if(cost<=minCost && !localMinPathWrappers.isEmpty()) {
				minCost = cost;
				pathWiseCost = localMinPathWrappers;
			}
			
		}
		if(pathWiseCost == null || pathWiseCost.size() == 0) {
			throw new Exception("Combinations not feasible with Cab capacity constraint !!!");
		}

		RoutePlan routePlan = generateRoutePlanFromMinPath(allRegistries, minCost, pathWiseCost,cabsWithCost);
		return routePlan;
	}

	private RoutePlan generateRoutePlanFromMinPath(Map<String, CabRegistry> allRegistries, int minCost,List<MinPathWrapperVO> pathWiseCost, Map<String, Integer> cabsWithCost) {
		RoutePlan routePlan = new RoutePlan();
		routePlan.setTotalCost(minCost);
		List<Route> routes = new ArrayList<>();
		for (MinPathWrapperVO pathWrapper : pathWiseCost) {
			Route route = new Route();
			route.setCabId(pathWrapper.getCabId());
			List<String> teamMemberIds = new ArrayList<>();
			Set<String> currentRoute = new HashSet<>();
			currentRoute.add("target_headquarter");
			
			for (Entry<String,Integer> entry : pathWrapper.getMinPath().entrySet()) {
				currentRoute.add(allRegistries.get(entry.getKey()).getDropPoint());
				teamMemberIds.add(entry.getKey());
			}
			route.setRoute(new LinkedList<>(currentRoute));
			route.setTeamMemberIds(teamMemberIds);
			route.setRouteCost(pathWrapper.getCost()*cabsWithCost.get(pathWrapper.getCabId()));
			routes.add(route);
		}
		
		routePlan.setRoutes(routes);
		return routePlan;
	}

	private Map<String, List<String>> getCabToRegisteredMemberMap(Map<String, String> map) {
		Map<String, List<String>> cabToDropPointMap = new HashMap<>();
		for (Entry<String,String> cabEntries : map.entrySet()) {
			List<String> dropPointsForThisCab = cabToDropPointMap.get(cabEntries.getValue());
			if(dropPointsForThisCab == null) {
				dropPointsForThisCab = new ArrayList<>();
			}
			dropPointsForThisCab.add(cabEntries.getKey());
			cabToDropPointMap.put(cabEntries.getValue(), dropPointsForThisCab);
		}
		return cabToDropPointMap;
	}

	@Override
	public void delete(RoutePlan s) {
		
	}

	@Override
	public List<RoutePlan> getAll() {
		return null;
	}

	@Override
	public List<RoutePlan> getAllById(RoutePlan s) {
		return null;
	}

}
