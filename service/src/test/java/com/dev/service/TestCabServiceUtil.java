package com.dev.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.dev.service.util.CabServiceUtil;

import junit.framework.Assert;

public class TestCabServiceUtil {
	
	@Test
	public void testPermutation() throws Exception{
		List<String> members = new ArrayList<>();
		members.add("A");
		members.add("B");
		members.add("C");
		List<List<String>> permutations = CabServiceUtil.getAllMemberPermutations(members);
		Assert.assertTrue(permutations.size()==6);
		Assert.assertEquals("[[A, B, C], [A, C, B], [B, A, C], [B, C, A], [C, A, B], [C, B, A]]", 
				permutations.toString());
	}
	
	@Test
	public void testCombinations() throws Exception{
		List<String> cabs = new ArrayList<>();
		cabs.add("C1");
		cabs.add("C2");
		List<String> members = new ArrayList<>();
		members.add("A");
		members.add("B");
		members.add("C");
		List<Map<String,String>> combination = CabServiceUtil.getAllCabCombinations(members, cabs);
		Assert.assertTrue(combination.size()==8);
	}

}
