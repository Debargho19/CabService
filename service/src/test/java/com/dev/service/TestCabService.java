package com.dev.service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dev.persistence.dao.CabDao;
import com.dev.persistence.dao.CabRegistryDao;
import com.dev.persistence.dao.DropPointDao;
import com.dev.pojo.impl.RoutePlan;
import com.dev.service.impl.RouteService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy(@ContextConfiguration)
public class TestCabService {

	@Configuration
	static class ContextConfiguration {

		@Bean
		public RouteService routeService() {
			RouteService routeService = new RouteService();
			return routeService;
		}

		@Bean
		public CabRegistryDao cabRegistryDao() {
			CabRegistryDao cabRegistryDao = new CabRegistryDao();
			return cabRegistryDao;
		}

		@Bean
		public DropPointDao dropPointDao() {
			DropPointDao dropPointDao = new DropPointDao();
			return dropPointDao;
		}

		@Bean
		public CabDao cabDao() {
			CabDao cabDao = new CabDao();
			return cabDao;
		}
	}

	@Autowired
	private RouteService routeService;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testCabRouteServiceNoCabsAdded() throws Exception {
		thrown.expect(Exception.class);
		thrown.expectMessage("No Cabs Added !!!");
		@SuppressWarnings("unused")
		RoutePlan plan = routeService.get(null);
	}
	
}
