package com.dev.pojo.impl;

import java.util.HashMap;
import java.util.Map;

public enum Gender {
	
	
	M{
		public String getValue() {
			return "M";
		}
	},F{
		public String getValue() {
			return "F";
		}
	};
	
	public abstract String getValue();
	
	private static Map<String,Gender> enumMap =  new HashMap<>();
	static {
		for (Gender gender : Gender.values()) { 
			enumMap.put(gender.getValue(), gender); 
		}
	}
	
	public static Gender getEnum(String value) {
		return enumMap.get(value);
	}
	
}
