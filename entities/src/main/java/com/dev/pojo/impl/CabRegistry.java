package com.dev.pojo.impl;

import com.dev.pojo.base.BaseCabEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CabRegistry extends BaseCabEntity {

	private static final long serialVersionUID = -1457016432221201220L;
	
	@JsonProperty("team_member_id")
	private String teamMemberId;
	
	private Gender gender;
	
	@JsonProperty("drop_point")
	private String dropPoint;
	
	public String getTeamMemberId() {
		return teamMemberId;
	}
	public void setTeamMemberId(String teamMemberId) {
		this.teamMemberId = teamMemberId;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getDropPoint() {
		return dropPoint;
	}
	public void setDropPoint(String dropPoint) {
		this.dropPoint = dropPoint;
	}
	
}
