package com.dev.pojo.impl.request;

import java.util.List;

import com.dev.pojo.base.BaseCabEntity;
import com.dev.pojo.impl.CabRegistry;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RegisterMemberRequest extends BaseCabEntity{

	private static final long serialVersionUID = -1923523773925244430L;
	
	@JsonProperty("team_members")
	private List<CabRegistry> teamMembers;

	public List<CabRegistry> getTeamMembers() {
		return teamMembers;
	}

	public void setTeamMembers(List<CabRegistry> teamMembers) {
		this.teamMembers = teamMembers;
	}
	
}
