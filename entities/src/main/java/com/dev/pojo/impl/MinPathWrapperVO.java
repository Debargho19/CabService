package com.dev.pojo.impl;

import java.util.Map;

public class MinPathWrapperVO {
	private String cabId;
	private Map<String,Integer> minPath;
	private int cost;
	private boolean isAllCabMembersFemale;
	
	public String getCabId() {
		return cabId;
	}
	public void setCabId(String cabId) {
		this.cabId = cabId;
	}
	public Map<String, Integer> getMinPath() {
		return minPath;
	}
	public void setMinPath(Map<String, Integer> minPath) {
		this.minPath = minPath;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public boolean isAllCabMembersFemale() {
		return isAllCabMembersFemale;
	}
	public void setAllCabMembersFemale(boolean isAllCabMembersFemale) {
		this.isAllCabMembersFemale = isAllCabMembersFemale;
	}
	@Override
	public String toString() {
		return "MinPathWrapperVO [cabId=" + cabId + ", minPath=" + minPath + ", cost=" + cost
				+ ", isAllCabMembersFemale=" + isAllCabMembersFemale + "]";
	}
}
