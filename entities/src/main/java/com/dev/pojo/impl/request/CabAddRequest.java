package com.dev.pojo.impl.request;

import java.util.List;

import com.dev.pojo.base.BaseCabEntity;
import com.dev.pojo.impl.Cabs;

public class CabAddRequest extends BaseCabEntity{
	
	private static final long serialVersionUID = -4904049088350751198L;
	private List<Cabs> cabs;

	public List<Cabs> getCabs() {
		return cabs;
	}

	public void setCabs(List<Cabs> cabs) {
		this.cabs = cabs;
	}
	
	
}
