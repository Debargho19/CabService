package com.dev.pojo.impl;

import java.util.List;

import com.dev.pojo.base.BaseCabEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RoutePlan extends BaseCabEntity {

	private static final long serialVersionUID = -4258060364254755728L;
	
	@JsonProperty("total_cost")
	private double totalCost;
	private List<Route> routes;
	
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public List<Route> getRoutes() {
		return routes;
	}
	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}
	
}
