package com.dev.pojo.impl;

import com.dev.pojo.base.BaseCabEntity;

public class Cabs extends BaseCabEntity {

	private static final long serialVersionUID = 1785919041912274343L;
	
	private String id;
	private Integer cost;
	private Integer capacity;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	
}
