package com.dev.pojo.impl;

import java.util.Map;

import com.dev.pojo.base.BaseCabEntity;

public class DropPoint extends BaseCabEntity{

	private static final long serialVersionUID = -6057512291236241280L;
	private String dropPointName;
	private Map<String,Integer> distance;
	
	public String getDropPointName() {
		return dropPointName;
	}
	public void setDropPointName(String dropPointName) {
		this.dropPointName = dropPointName;
	}
	public Map<String, Integer> getDistance() {
		return distance;
	}
	public void setDistance(Map<String, Integer> distance) {
		this.distance = distance;
	}
	
}
