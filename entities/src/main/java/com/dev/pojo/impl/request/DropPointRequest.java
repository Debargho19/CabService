package com.dev.pojo.impl.request;

import java.util.Map;

import com.dev.pojo.base.BaseCabEntity;

public class DropPointRequest extends BaseCabEntity{

	private static final long serialVersionUID = -3650636889291952398L;
	private Map<String,String> dropPointsDistance;
	
	public Map<String, String> getDropPointsDistance() {
		return dropPointsDistance;
	}
	public void setDropPointsDistance(Map<String, String> dropPointsDistance) {
		this.dropPointsDistance = dropPointsDistance;
	}
	
	
	
}
