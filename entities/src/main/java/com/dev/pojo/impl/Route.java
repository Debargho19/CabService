package com.dev.pojo.impl;

import java.util.List;

import com.dev.pojo.base.BaseCabEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Route extends BaseCabEntity{

	private static final long serialVersionUID = -6559022003498057045L;
	
	@JsonProperty("cab_id")
	private String cabId;
	
	@JsonProperty("team_member_ids")
	private List<String> teamMemberIds;
	
	private List<String> route;
	
	@JsonProperty("route_cost")
	private int routeCost;
	
	public String getCabId() {
		return cabId;
	}
	public void setCabId(String cabId) {
		this.cabId = cabId;
	}

	public List<String> getTeamMemberIds() {
		return teamMemberIds;
	}
	public void setTeamMemberIds(List<String> teamMemberIds) {
		this.teamMemberIds = teamMemberIds;
	}
	public List<String> getRoute() {
		return route;
	}
	public void setRoute(List<String> route) {
		this.route = route;
	}
	public int getRouteCost() {
		return routeCost;
	}
	public void setRouteCost(int routeCost) {
		this.routeCost = routeCost;
	}
	
}
