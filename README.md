## CAB Allocation Service

Cab service is a system to find OPTIMAL routes for team members to drop them to drop-points nearest to their home.
Each team member has to register to the travel portal to avail this service and select the drop point.
Travel team staff can configure the number of cabs available along with their cost and capacity. 
They also configure the available drop points, and their distance from target headquarters and also the distance from one drop point to another.


This example uses a in-memory Map to store the data and can be replaced with some actual persistence store.

## Steps to clone and start the service

### Clone

git clone git@gitlab.com:Debargho19/CabService.git

### Build

cd /CabService

mvn clean install

### Start the service

java -jar api/target/cab-api.jar server api/src/main/resources/configuration.yml


APIs :

1)	POST /register
```
    {
    	"team_members": [
    	    {
    			"team_member_id": "1",
    			"gender": "F",
    			"drop_point": "pointC"
    		},
    		{
    			"team_member_id": "2",
    			"gender": "F",
    			"drop_point": "pointB"
    		},
    		{
    			"team_member_id": "3",
    			"gender": "F",
    			"drop_point": "pointA"
    		},
    		{
    			"team_member_id": "4",
    			"gender": "M",
    			"drop_point": "pointE"
    		},
    		{
    			"team_member_id": "5",
    			"gender": "M",
    			"drop_point": "pointE"
    		}
    	]
    }
```
    
    
2)	POST /cabs
```
    {
    	"cabs": [
    	    {
    			"id": "cab1",
    			"cost": 2,
    			"capacity": 2
    		},
    		{
    			"id": "cab2",
    			"cost": 1,
    			"capacity": 3
    		}
    	]
    
    }
```

3)	POST /drop_points
```
    {
     	"target_headquarter": "1,8,1,2,1",
     	"pointA": "0,1,2,1,2",
     	"pointB": "8,0,1,3,1",
     	"pointC": "7,9,0,1,1",
     	"pointD": "2,2,2,0,1",
     	"pointE": "2,9,6,7,0"
     }
```
     
4)	GET /routeplan
    
    Sample Response:
```
    
    {
    	"routes": [{
    			"route": [
    				"target_headquarter",
    				"pointE",
    				"pointA",
    				"pointB"
    			],
    			"cab_id": "cab2",
    			"team_member_ids": [
    				"3",
    				"2",
    				"5"
    			],
    			"route_cost": 3
    		},
    		{
    			"route": [
    				"target_headquarter",
    				"pointE",
    				"pointC"
    			],
    			"cab_id": "cab1",
    			"team_member_ids": [
    				"1",
    				"4"
    			],
    			"route_cost": 4
    		}
    	],
    	"total_cost": 7
    }
```

